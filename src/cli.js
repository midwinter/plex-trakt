import minimist from 'minimist';
import chalk from 'chalk';
import prompts from 'prompts';
import Trakt from 'trakt.tv';
import * as PlexService from './PlexService'
import fs from 'fs'

export async function cli(argsArray) {
    const args = minimist(argsArray.slice(2));
    let cmd = args._[0] || 'help';

    if (args.version || args.v) {
        cmd = 'version';
    }
    //console.log(JSON.stringify(args));

    //PlexService.listMedia()

    if(args.init || args.i) {
        generateConfig();
    }
}

async function generateConfig() { 
    console.log(chalk.blue('Generating Config'));
    const trakt = await generateTraktConfig();
    const plex = await generatePlexConfig();
    fs.writeFile('config.json', JSON.stringify({plex, trakt}, null, 2), 'utf8',()=>{
        console.log('config written to config.json');
    });

}
async function generatePlexConfig() {

    return await prompts([
        {
            type: 'text',
            name: 'host',
            message: 'What is the hostname/ip of your plex server?'
        },
        {
            type: 'number',
            name: 'port',
            message: 'What is the port of your plex server?', 
            initial: 32400
        },
        {
            type: 'text',
            name: 'username',
            message: 'What is your plex username/email?'
        },
        {
            type: 'password',
            name: 'password',
            message: 'What is your plex password?'
        },
    ]);


}
async function generateTraktConfig() {

    console.log('You will have to generate a client_id and secret. Visit https://trakt.tv/oauth/applications and create a new app(for the url you can enter\'http://localhost/\')')
    const traktResponses = await prompts([
        {
            type: 'text',
            name: 'client_id',
            message: 'What is the client_id of your application?'
        },
        {
            type: 'text',
            name: 'client_secret',
            message: 'What is the client_secret of your application?'
        },
    ])
    const { client_id, client_secret } = traktResponses;
    const auth  = await getTraktAuth(client_id, client_secret);
    return {auth, client_id, client_secret }
}

async function getTraktAuth(client_id, client_secret) {
    try {
        const trakt = new Trakt({client_id, client_secret});
        const poll = await trakt.get_codes()
        console.log(`visit ${poll.verification_url} and enter ${poll.user_code}`);

        // poll.verification_url: url to visit in a browser
        // poll.user_code: the code the user needs to enter on trakt
        // verify if app was authorized
        return trakt.poll_access(poll);
    } catch (error) {
        console.log(error)
        // error.message == 'Expired' will be thrown if timeout is reached
    };
}
