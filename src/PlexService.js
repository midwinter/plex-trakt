
import PlexAPI from 'plex-api'

function getPlexClient() {
    return new PlexAPI()
}

const plexClient = getPlexClient();

async function getMediaLibraryData() {
    try {
        const sections = await plexClient.query("/library/sections", {type: "shows"})
        return sections.MediaContainer.Directory;
    } catch (e) {
        console.log(e)
        return
    }
}

export async function listMedia() {
    const mediaData  = await getMediaLibraryData();
    mediaData.forEach(async(library)=> {
        //console.log(JSON.stringify(library ));
        const data = await plexClient.query("/library/sections/" + library.key + "/all")
        console.log(library.title);
        data.MediaContainer.Metadata.forEach(item => {
            if(item.type === 'movie') {
                processMovie(item);
            } else if (item.type === 'show') {
                processTv(item)
            } else {

            console.log(JSON.stringify(item.type))
            }
        })

    });
}

async function processTv (tv) {
    return;
    console.log(tv.title);
    //console.log(JSON.stringify(tv, null, 2))
        //const data = await plexClient.query("/library/metadata/" + tv.ratingKey + "/children")
        const data = await plexClient.query(tv.key)
    data.MediaContainer.Metadata.forEach(processTVSeason)

}
async function processTVSeason(season) {
    console.log(season.parentTitle + " " + season.title);
        const data = await plexClient.query(season.key)
    //console.log(JSON.stringify(data, null, 2))
    data.MediaContainer.Metadata.forEach(processTVEpisode)
}
async function processTVEpisode(episode) {
    console.log(JSON.stringify(episode, null, 2))
}
async function processMovie (movie) {
    //console.log(JSON.stringify(movie, null, 2))
    const viewed = !!movie.viewCount
    if(viewed) {
        processWatchedMovie(movie)
        console.log(movie.title)
    } else {
        processUnwatchedMovie(movie)
        return
    }

    //const data = await plexClient.query(movie.key)
    //console.log(JSON.stringify(data, null, 2))
}

function processUnwatchedMovie() {

}
function processWatchedMovie() {

}
